package com.tema5.notificationssample;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static com.tema5.notificationssample.MainActivity.CHANNEL_ID4;

/**
 * Created by Sergio on 23/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFCMService";

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            String title = remoteMessage.getData().get("title");
            String body = remoteMessage.getData().get("body");
            notifBasic(title, body);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            RemoteMessage.Notification notification = remoteMessage.getNotification();

        }

    }

    private void notifBasic(String title, String body) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID4)
                .setSmallIcon(R.drawable.ic_stat_name) //icono pequeño
                .setContentTitle(title) //titulo
                .setContentText(body) //texto
                .setPriority(NotificationCompat.PRIORITY_DEFAULT); //prioridad para android 7.1 y anteriores

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this); //llamamos al gestor de notificaciones
        notificationManager.notify(5, builder.build()); //lanzamos con el gestor la notificación, bajo una id que la identifica
    }
}
