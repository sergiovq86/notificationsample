package com.tema5.notificationssample;

import androidx.core.app.Person;

public class Message {

    private String text;
    Person person;
    long time;

    public Message(String text, Person person, long time) {
        this.text = text;
        this.person = person;
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}

