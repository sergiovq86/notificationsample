package com.tema5.notificationssample;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.Person;
import androidx.core.app.TaskStackBuilder;
import androidx.core.graphics.drawable.IconCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    public static final String CHANNEL_ID = "myNotif";
    public static final String CHANNEL_ID3 = "myNotif2";
    public static final String CHANNEL_ID4 = "myNotif4";
    private static final int notificationId = 5;

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createNotificationChannel();
        createNotificationChannelNoBadge();
        createCustomChannel();

        findViewById(R.id.notif_basic).setOnClickListener(this);
        findViewById(R.id.notif_push).setOnClickListener(this);
        findViewById(R.id.notif_cancel).setOnClickListener(this);
        findViewById(R.id.notif_big_img).setOnClickListener(this);
        findViewById(R.id.notif_big_little_img).setOnClickListener(this);
        findViewById(R.id.notif_big_text).setOnClickListener(this);
        findViewById(R.id.notif_bandeja).setOnClickListener(this);
        findViewById(R.id.notif_conversacion).setOnClickListener(this);
        findViewById(R.id.notif_multimedia).setOnClickListener(this);
        findViewById(R.id.notif_pending_normal).setOnClickListener(this);
        findViewById(R.id.notif_pending_especial).setOnClickListener(this);
        findViewById(R.id.notif_custom).setOnClickListener(this);
        findViewById(R.id.notif_sound).setOnClickListener(this);

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Fallo al obtener el token de registro", task.getException());
                            return;
                        }

                        // Token de Registro FCM
                        String token = task.getResult();

                        // Log y toast
                        String msg = "Token: " + token;
                        Log.d(TAG, msg);
                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.notif_basic:
                notifBasic();
                break;

            case R.id.notif_push:
                notifPush();
                break;

            case R.id.notif_cancel:
                cancelNotif();
                break;

            case R.id.notif_big_img:
                notifImagenGrande();
                break;

            case R.id.notif_big_little_img:
                notifImagenGrandeYMiniatura();
                break;

            case R.id.notif_big_text:
                notifTextGrande();
                break;

            case R.id.notif_bandeja:
                notifBandejaEntrada();
                break;

            case R.id.notif_conversacion:
                notifConversacion();
                break;

            case R.id.notif_multimedia:
                notifMultimedia();
                break;

            case R.id.notif_pending_normal:
                pendingIntentNormal();
                break;

            case R.id.notif_pending_especial:
                pendingIntentEspecial();
                break;

            case R.id.notif_custom:
                customNotif();
                break;

            case R.id.notif_sound:
                soundNotification();
                break;
        }
    }

    private void notifBasic() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name) //icono pequeño
                .setContentTitle("Notificación básica") //titulo
                .setContentText("Esta es una notificación básica") //texto
                .setPriority(NotificationCompat.PRIORITY_DEFAULT); //prioridad para android 7.1 y anteriores

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this); //llamamos al gestor de notificaciones
        notificationManager.notify(notificationId, builder.build()); //lanzamos con el gestor la notificación, bajo una id que la identifica
    }

    private void notifPush() {
        Intent intent = new Intent(this, SecondActivity.class); //intent con la actividad a abrir
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); //flags que configuran la actividad que se inicia
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0); //pending intent
        //requestCode es un código que permite identificar el pendingintent que ha abierto la actividad
        //el 4º parámetro permite configurar el intent con flags
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle("Notificación con toque")
                .setContentIntent(pendingIntent) //asigna el pending intent
                .setAutoCancel(true) //autocancela al pulsar la notificación
                .setContentText("Pulsa para abrir la actividad.")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, builder.build());
    }

    private void cancelNotif() {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.cancel(notificationId); //elimina una notificación con dicha id de la barra de notificaciones
    }

    private void notifImagenGrande() {
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.milhouse); //bitmap de la imagen a mostrar
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle(); //se crea un objeto BigPictureStyle
        bigPictureStyle.bigPicture(bm); // se asigna una imagen

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle("Expandible Imagen Grande")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentText("Ésta es una notificación expandible con imagen grande")
                .setStyle(bigPictureStyle); //se indica la imagen grande a mostrar

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, builder.build());
    }

    private void notifImagenGrandeYMiniatura() {
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.milhouse);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle("Expandible Imagen Grande")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setLargeIcon(bm) //se pasa el bitmap para mostrar cuando la notificación esté contraíta
                .setContentText("Ésta es una notificación expandible con imagen grande")
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bm)
                        .bigLargeIcon(null)); //Large icon desaparece al expandir, y vuelve a aparecer al contraer

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, builder.build());
    }

    private void notifTextGrande() {
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.milhouse);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle("Expandible Texto Grande")
                .setLargeIcon(bm)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentText("Notificación expandible con bloque grande de texto") // se muestra con la notificación contraida
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(getString(R.string.texto_largo))); //texto largo al expandir la notificación

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, builder.build());
    }

    private void notifBandejaEntrada() {
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.milhouse);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle("Bandeja de entrada")
                .setLargeIcon(bm)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentText("Tienes 2 mensajes") // se muestra con la notificación contraida
                .setStyle(new NotificationCompat.InboxStyle()
                        .addLine(getString(R.string.texto_largo)) //cada linea separada a mostrar en la notifiacción
                        .addLine(getString(R.string.texto_largo2)));

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, builder.build());
    }

    private void notifConversacion() {
        // Persona 1
        Person.Builder pBuilder = new Person.Builder().setName("Milhouse")
                .setIcon(IconCompat.createWithResource(this, R.drawable.milhouse));
        Person person1 = pBuilder.build();

        // Persona 2
        Person.Builder pBuilder2 = new Person.Builder().setName("Dr. Zoidberg")
                .setIcon(IconCompat.createWithResource(this, R.drawable.zoidberg));
        Person person2 = pBuilder2.build();

        // Creamos fechas para los mensajes
        Calendar calendar = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar2.add(Calendar.MINUTE, -3);

        // notificación 1, muestra los mensajes de la persona 1
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setStyle(new NotificationCompat.MessagingStyle(person1)
                        // añade mensaje: texto, fecha en formato long y person
                        .addMessage("Todo ha salido a pedir de Milhouse", calendar2.getTimeInMillis(), person1))
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, notification);

        // notificación 2 para los mensajes de la persona 2
        Notification notification2 = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setStyle(new NotificationCompat.MessagingStyle(person2)
                        // añade dos mensajes
                        .addMessage("¡No hay nada como la primera taza de jugo de basura en la mañana!", calendar.getTimeInMillis(), person2)
                        .addMessage("Por cierto, me tomé la libertad de fertilizar tu caviar.", calendar2.getTimeInMillis(), person2))
                .build();

        notificationManager.notify(25, notification2);

        // mezcla mensajes de distintas personas en la misma notificación
        Notification notification3 = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setStyle(new NotificationCompat.MessagingStyle(person1)
                        .addMessage("¡No hay nada como la primera taza de jugo de basura en la mañana!", calendar.getTimeInMillis(), person1)
                        .addMessage("Por cierto, me tomé la libertad de fertilizar tu caviar.", calendar2.getTimeInMillis(), person2))
                .build();

        notificationManager.notify(26, notification3);
    }

    private void notifMultimedia() {
        Bitmap nirvana = BitmapFactory.decodeResource(getResources(), R.drawable.nirvana); //imagen del disco en grande

        // conjnto de pending Intent que realizan una acción determinada
        Intent addIntent = new Intent(this, MainActivity.class);
        PendingIntent prevPendingIntent =
                PendingIntent.getService(this, 0, addIntent, 0);
        PendingIntent pausePendingIntent =
                PendingIntent.getService(this, 0, addIntent, 0);
        PendingIntent nextPendingIntent =
                PendingIntent.getService(this, 0, addIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID3)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) // Muestra los controles cuando la App está bloqueada
                .setSmallIcon(R.drawable.ic_stat_name)
                // Controles multimedia
                .addAction(R.drawable.ic_previous, "Previous", prevPendingIntent) // #id 0
                .addAction(R.drawable.ic_pause, "Pause", pausePendingIntent)  // #id 1
                .addAction(R.drawable.ic_next, "Next", nextPendingIntent)     // #id 2
                // Aplica el estilo multimedia
                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                        .setShowActionsInCompactView(1)) //en la vista contraída se mostrará el botón con id 1
                .setContentTitle("Wonderful music")
                .setContentText("My Awesome Band")
                .setLargeIcon(nirvana)
                .build();
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(25, notification);

    }

    private void pendingIntentNormal() {
        Intent resultIntent = new Intent(this, SecondActivity.class); //intent para la actividad a abrir
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this); // permite crear una pila de actividades
        stackBuilder.addNextIntentWithParentStack(resultIntent); // se incluye el intent. En Manifest ya se ha indicado cuál es la actividad padre
        PendingIntent resultPending =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT); //pendingintent normal, el Flag indica que si ya existe el pendingintent, se actualice

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle("PendingIntent")
                .setContentIntent(resultPending)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentText("PendingIntent abre actividad normal");

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, builder.build());
    }

    private void pendingIntentEspecial() {
        Intent notifyIntent = new Intent(this, SecondActivity.class);  //intent para la actividad a abrir
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); //con estos flags se consigue abrir una actividad nueva sin actividades padre
        // también se ha configurado la actividad en el manifest
        PendingIntent notifyPending = PendingIntent.getActivity(
                this, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
        );

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle("PendingIntent")
                .setContentIntent(notifyPending)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentText("PendingIntent abre actividad especial");

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, builder.build());
    }

    private void customNotif() {
        // layouts para la notificación personalizada
        RemoteViews collapsedView = new RemoteViews(getPackageName(), R.layout.notification_small); //carga layout
        collapsedView.setTextViewText(R.id.content_title, "Notificación personalizada"); //pone texto en textview con esta id
        collapsedView.setTextViewText(R.id.content_text, "Esta es una notificación personalizada");
        collapsedView.setTextViewText(R.id.timestamp, DateUtils.formatDateTime(this, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME)); //timestamp
        RemoteViews expandedView = new RemoteViews(getPackageName(), R.layout.notification_large);
        expandedView.setImageViewResource(R.id.big_icon, R.drawable.nirvana);
        expandedView.setTextViewText(R.id.timestamp, DateUtils.formatDateTime(this, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME));
        expandedView.setTextViewText(R.id.notification_message, "Esta es una notificación personalizada");
        Intent intent = new Intent(this, SecondActivity.class);
        // click en botón izquierdo abre actividad
        expandedView.setOnClickPendingIntent(R.id.left_button, PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));

        // Apllica los layouts a la notificación
        Notification customNotification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle()) //necesario para usar una vista personalizada
                .setCustomContentView(collapsedView) //indica la vista contraida
                .setCustomBigContentView(expandedView) //indica la vista expandida
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle("Expandible Imagen Grande")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentText("Ésta es una notificación expandible con imagen grande")
                .build();
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, customNotification);
    }

    private void soundNotification() {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.milhouse);

        // el canal está configurado para cambiar vibración, sonido y luces
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID4)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle("Expandible Imagen Grande")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentText("Ésta es una notificación expandible con imagen grande")
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bm));

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, builder.build());

    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channel_name = "Notificaciones de prueba";//nombre canal
            String channel_description = "Probando notificaciones en una App de ejemplo";//descripción
            int importance = NotificationManager.IMPORTANCE_DEFAULT;//Prioridad o importancia de la notificación
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, channel_name, importance); //construye el canal
            channel.setDescription(channel_description); // añade una descripción
            NotificationManager notificationManager = getSystemService(NotificationManager.class); //Se obtiene el gestor de notificaciones
            notificationManager.createNotificationChannel(channel); //se crea el canal de notificaciones
        }
    }

    private void createNotificationChannelNoBadge() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Notificación sin insignia";
            String description = "Canal de notificaciones sin ingisnia";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID3, name, importance);
            channel.setDescription(description);
            channel.setShowBadge(false); // para no mostrar el indicador de nuevas notificaciones sobre el icono
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void createCustomChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Canal personalizado";
            String description = "Canal de notificaciones personalizado";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID4, name, importance);
            channel.setDescription(description);
            channel.setLightColor(Color.RED); //establece un color al led de notificaciones
            // patrón de vibración
            channel.setVibrationPattern(new long[]{100, 30, 100, 30, 100, 200, 200, 30, 200, 30, 200, 200, 100, 30, 100, 30, 100, 100, 30, 100, 30, 100, 200, 200, 30, 200, 30, 200, 200, 100, 30, 100, 30, 100});
            Uri uri = Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.coconuts); //uri del sonido

            //constructor del sonido
            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();
            channel.setSound(uri, att); //establece el sonido a la aplicación

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            NotificationChannel channel1 = notificationManager.getNotificationChannel(CHANNEL_ID);
            Uri sounUri = channel1.getSound();
        }
    }
}